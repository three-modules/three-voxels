import { Vector3, Box3, Matrix4 } from "three";
import { SimplexNoise } from "three/examples/jsm/math/SimplexNoise";

/**
 * VoxelObjects 
 * 
 * An object defined by an isosurface
 */
export class VoxelObjects {

    // field function
    static noise = (box: Box3, simplex: SimplexNoise) => {
        return {
            isosurface: (p: Vector3, t = 0) => {
                var p2 = p.clone();//.multiplyScalar(0.25);
                var freq = [0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8];
                var noise = 0;
                for (var i = 0; i < freq.length; i++) {
                    noise += (simplex.noise4d(p2.x * freq[i], p2.z * freq[i], p2.y / 16, t) + 0.5) / Math.pow(2, i + 1);//
                }
                return noise;

            },
            isoLvl: 0.4,
            boundaries: box,
            vxdensity: 0.125
        }
    }

    // BASIC: implicit functions

    static sphere = (radius: number, vxdensity: number, isFilled = true, isoLvl = 0.4) => {
        return {
            isosurface: (p: Vector3, t?: number) => {
                var dist = p.length(); //p.distanceTo(center);
                var voxelSize = 1 / vxdensity;
                // var weigth = dist <= (radius + 2 * voxelSize) ? Math.pow(radius / dist, 2) : 0;
                var val;

                if (isFilled) {
                    if (dist <= radius) {
                        val = (radius - dist) * (1 - isoLvl) / radius + isoLvl;
                    } else {
                        val = isoLvl * (1 - (dist - radius) / dist);
                    }
                    // return dist > 0.01 ? radius * isoLvl / dist : 100;
                } else {
                    if (dist <= radius) {
                        val = dist * isoLvl / radius;
                    } else {
                        val = isoLvl + (1 - isoLvl) * (dist - radius) / dist;
                    }
                }
                return val;
            },
            filled: isFilled,
            isoLvl: 0.4,
            boundaries: new Box3(new Vector3(- radius, - radius, - radius),
                new Vector3(radius, radius, radius)),
            vxdensity: vxdensity
        }
    }

    static cylinder = (radius: number, height: number, vxdensity: number, isFilled = true, isoLvl = 0.4) => {
        return {
            isosurface: (p: Vector3, t?: number) => {
                var p2 = new Vector3(p.x, 0, p.z);
                return VoxelObjects.sphere(radius, vxdensity, isFilled).isosurface(p2);
            },
            isoLvl: isoLvl,
            boundaries: new Box3(new Vector3(- radius, - height / 2, - radius),
                new Vector3(radius, height / 2, radius)),
            vxdensity: 0.5,
            matrix: new Matrix4()
        }
    }

    static box = (length: number, width: number, height: number) => {
        return {
            isosurface: (vp: Vector3, t: number) => {
                // var v = p.clone().add(origin.clone().multiplyScalar(-1));
                return ((vp.x > -length / 2) && (vp.x < length / 2)) &&
                    ((vp.y > -height / 2) && (vp.y < height / 2)) &&
                    ((vp.z > -width / 2) && (vp.z < width / 2))
            },
            isoLvl: 0.4,
            boundaries: new Box3(new Vector3(- length / 2, - height / 2, - width / 2),
                new Vector3(length / 2, height / 2, width / 2)),
            vxdensity: 0.5
        }
    }


    // META OBJECTS (composition of basic objects)
    // Tower: 1 cylinder inside another
}