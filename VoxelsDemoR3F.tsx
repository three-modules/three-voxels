import * as THREE from 'three'
import React, { Suspense, useEffect, useRef, useState } from 'react'
import { Canvas, useFrame, useLoader } from '@react-three/fiber'
import { Box3, Vector3 } from 'three';
import { useVoxelStates } from './VoxelStates';
import { VoxelRenderer } from './VoxelRenderer';
import { OctreeBrowserHlp, VoxelModesSwitcher, VOXEL_MODES } from './VoxelHelpers';
import { VoxelEntitiesWrapper, VoxelRenderWrapper } from './VoxelWrappers';
import { VoxelFactory } from './VoxelFactory';
import { VoxelObjects } from './VoxelObjects';
// import { Wrapper, Controls, Lights } from '../../ext-deps/samples/BasicDemo';
import { Controls, Lights } from '../../ext-deps/BasicDemo'
import { BoxEntity } from '../../ext-deps/modules/helpers/BoxEntityCtrlHlp';
import * as MaterialCatalog from "../three-resources/catalogs/Materials"
import { SimplexNoise } from 'three/examples/jsm/math/SimplexNoise';



const Static = () => {
    console.log("Static")
    const incEntities = useVoxelStates(state => state.incEntities);
    if (!Object.keys(VoxelFactory.entities).length)
        proceduralCavernsSplitted();
        // proceduralCaverns()

    useEffect(() => {
        console.log("DBG")
        incEntities();
    }, []);

    return (<>
        {/* <group>{grp}</group>; */}
    </>)
}

const Helpers = () => {
    const wrappers = useVoxelStates(state => state.entities.wrappers);
    const selectedEntityId = wrappers.findIndex((ent: BoxEntity) => ent.selected);
    return (<>
        <VoxelModesSwitcher initialMode={VOXEL_MODES.CREATE} />
        <OctreeBrowserHlp octree={VoxelRenderer.renderOctree} entityId={selectedEntityId} />
        {/* <VoxelHelpers size={128} /> */}
        {/* <group>{grp}</group>; */}
    </>)
}

export const VoxelsDemoR3F = ({ style }: any) => {
    console.log("voxel demo")
    const size = 128;
    const renderBox = new Box3(new Vector3(-size, -size, -size),
        new Vector3(size, size, size));


    return (
        <>
            <h2>Voxels Demo!</h2>
            {/* <TextureHelper texData={null} size={size} /> */}
            {/* <InfoOverlay sample={sample} /> */}
            <Canvas camera={{ position: [15, 30, 50] }} style={style} shadows
                onCreated={({ gl }) => {
                    gl.setClearColor(new THREE.Color('#020207'))
                }}>
                <ambientLight intensity={0.5} />
                {/* <Wrapper /> */}
                <Controls />
                <Lights />
                <Static />
                <VoxelRenderWrapper renderBox={renderBox} />
                {/* <VoxelEntitiesWrapper /> */}
                {/* <Helpers /> */}
            </Canvas>
        </>
    )
};

const getMatrix: any = (posx: number, posy: number, posz: number, scale: number) => {
    var matrix = new THREE.Matrix4();
    var matScale = new THREE.Matrix4().makeScale(scale, scale, scale);
    var matTrans = new THREE.Matrix4().makeTranslation(posx, posy, posz);
    matrix.multiplyMatrices(matScale, matTrans);
    return matrix;
}

const proceduralCaverns: any = () => {
    var simplex = new SimplexNoise();

    var matlFn = () => {
        var mat: any = MaterialCatalog.ShaderTriplTexColBlend();
        mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0));
        // mat = MaterialCatalog.WATER();
        mat.side = THREE.DoubleSide;
        return mat;
    }

    var size = 64; var chunkSize = size / 2; var range = size / chunkSize;

    var rangeBox = new Box3(new Vector3(-size, -size, -size),
        new Vector3(size, size, size));
    var noiseMatrix = getMatrix(0, 0, 0, 4);
    var noiseMatrixInv = noiseMatrix.clone().getInverse(noiseMatrix);
    var noiseBox = rangeBox.clone().applyMatrix4(noiseMatrixInv);
    VoxelFactory.createEntity(VoxelObjects.noise(noiseBox, simplex), matlFn, noiseMatrix);

    // var radius = 4 * 2 * Math.PI; var d = 0;
    // var translatMat = getMatrix(d, d, d, 1);
    // VoxelFactory.createEntity(VoxelObjects.sphere(radius, 1 / 4, false), matlFn, translatMat);

}

const proceduralCavernsSplitted: any = () => {
    var simplex = new SimplexNoise();

    var matlFn = () => {
        var mat: any = MaterialCatalog.ShaderTriplTexColBlend();
        mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0));
        // mat = MaterialCatalog.WATER();
        mat.side = THREE.DoubleSide;
        return mat;
    }

    var size = 128; var chunkSize = size / 2; var range = size / chunkSize;
    var dim = (new THREE.Vector3(1, 1, 1)).multiplyScalar(chunkSize);
    var count = 0;

    for (var y = -range / 2; y < range / 2; y++) {
        // console.log("Y from " + rangeBox.min.y + " to " + rangeBox.max.y);
        for (var z = -range / 2; z < range / 2; z++) {
            // console.log("Z from " + rangeBox.min.z + " to " + rangeBox.max.z);
            for (var x = -range / 2; x < range / 2; x++) {
                if (count < 8) {
                    console.log("X: " + x)
                    var translatMat = getMatrix(x * size, y * size, z * size, 1);
                    // rangeBox = new THREE.Box3(new THREE.Vector3(-chunkSize / 2, -chunkSize / 2, -chunkSize / 2), new THREE.Vector3(chunkSize / 2, chunkSize / 2, chunkSize / 2));
                    var rangeBox = new THREE.Box3(new THREE.Vector3(0, 0, 0), new THREE.Vector3(size, size, size));
                    rangeBox.applyMatrix4(translatMat);
                    console.log("X from " + rangeBox.min.x + " to " + rangeBox.max.x);
                    // VoxelFactory.createEntity(VoxelObjects.noise(rangeBox, simplex), matlFn, getMatrix(0, 0, 0, 1));
                    var noiseMatrix = getMatrix(0, 0, 0, 4);
                    var noiseMatrixInv = noiseMatrix.clone().getInverse(noiseMatrix);
                    var noiseBox = rangeBox.clone().applyMatrix4(noiseMatrixInv);
                    VoxelFactory.createEntity(VoxelObjects.noise(noiseBox, simplex), matlFn, noiseMatrix);

                }
                count++
            }
        }
    }
    var radius = 4 * 2 * Math.PI; var d = 0;
    var translatMat = getMatrix(d, d, d, 1);
    VoxelFactory.createEntity(VoxelObjects.sphere(radius, 1 / 4, false), matlFn, translatMat);
}

// hole in the ground
const rabbitHole: any = () => {
    var matlFn = () => {
        var mat: any = MaterialCatalog.ShaderTriplTexColBlend();
        mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0));
        // mat = MaterialCatalog.WATER();
        mat.side = THREE.BackSide;
        return mat;
    }

    var radius = 32 * 2 * Math.PI;
    var offset = 32;
    var translatMat;

    // earth = big sphere
    translatMat = getMatrix(0, -radius * 4, 0, 1);
    VoxelFactory.createEntity(VoxelObjects.sphere(radius * 4, 1 / 4, true), matlFn, translatMat);

    translatMat = getMatrix(0, 40, 0, 1);
    VoxelFactory.createEntity(VoxelObjects.sphere(radius / 16, 1 / 4, true), matlFn, translatMat);
    // translatMat = getMatrix(0, 17, 0, 1);
    // VoxelFactory.createEntity(VoxelObjects.sphere(radius / 16, 1 / 4, false), matlFn, translatMat);
    translatMat = getMatrix(0, 0, 0, 1);
    VoxelFactory.createEntity(VoxelObjects.sphere(radius / 16, 1 / 4, false), matlFn, translatMat);
    // translatMat = getMatrix(-10, -10, 0, 1);
    // VoxelFactory.createEntity(VoxelObjects.sphere(radius / 16, 1 / 4, false), matlFn, translatMat);
    // translatMat = getMatrix(-20, -20, 0, 1);
    // VoxelFactory.createEntity(VoxelObjects.sphere(radius / 16, 1 / 4, false), matlFn, translatMat);




    // VoxelFactory.createRenderer(rangeBox);
}